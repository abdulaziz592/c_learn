# Clearn (Learn C language with E-Learning)

### Why/What is Clearn ? 

Several universities form help desks for the students to understand computer programming in addition to assigned labs hours.

Studies show low passing rates in programming courses especially when this is the first programmingcourse,
The student having clear understanding of the programming constructs can apply it to solve various problems.
Our project will provide 24-hours help desk that will guide the students to there first programing course (c language ).

The necessity of designing e-learning course to help novice learners is due to the dynamic nature of computer programming problems.

One of the main problems in this regard is the limited time of the instructor given in theory and lab classes to cover the syllabus.

In our system student will learn by reading/solving assessments and the system will guide the student through sections by our backtracking engine.