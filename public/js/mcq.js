/**
 * Created by Sohib on 2/22/15.
 */
$.fn.shuffleChildren = function () {
    $.each(this.get(), function (index, el) {
        var $el = $(el);
        var $find = $el.children();

        $find.sort(function () {
            return 0.5 - Math.random();
        });

        $el.empty();
        $find.appendTo($el);
    });
};

$(".questions").shuffleChildren();
$(".answers").shuffleChildren();
//$(".true-sign,.false-sign").hide();



//Callback handler for form submit event
$("#quiz").submit(function (e) {

    var formObj = $(this);
    var formURL = formObj.attr("action");
    var formData = new FormData(this);

    $.ajax({
        url: formURL,
        type: 'POST',
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data, textStatus, jqXHR) {
            $(".radio").removeClass("has-success has-error");
            $("label.panel").removeClass("panel-success panel-danger");
            $(".true-sign,.false-sign").hide();
           // console.log(data)
            jsoneRespone = $.parseJSON(data);
            precentage = jsoneRespone.precentage;
            grade = jsoneRespone.grade;
            $.each(jsoneRespone.answers, function () {
                console.log(jsoneRespone);
                id = this.id;
                result = this.result;

                //radioParent = $('input[name=' + id + ']:checked', '#quiz').parent().parent();

                // get correct ansower
                liRadioParent = $('input[name="' + id + '"]', '#quiz');
                // panel of correct answer
                panel = liRadioParent.parents("label.panel");
                // controls of panel
                panelControls = panel.find(".panel-controls");
                // signs of correct ansower panel
                trueSign = panelControls.children(".true-sign");
                falseSign = panelControls.children(".false-sign");

                if (result == true) {
                    liRadioParent.addClass("has-success");
                    panel.addClass("panel-success");
                    trueSign.show();
                } else {
                    liRadioParent.addClass("has-error");
                    panel.addClass("panel-danger");
                    falseSign.show();
                }
//
                console.log(textStatus);
            });

            $('.mb-title').text(grade);

            message = " your grade is " + grade + "because you solved only " + precentage + "% <br>" +
            "Please read these chapter before your 2nd attempt <br>";
            list = "";
            titles = [];
            list += "<ul>";
            $.each(jsoneRespone.answers, function () {
                if($.inArray(this.back_tracking_title,titles) != -1)
                    return;
                if(this.result)
                    return;
                titles.push( this.back_tracking_title);
                list += "<li><a href='"+this.back_tracking_url+"'>"+this.back_tracking_title+"</a></li>"
            } );
            list += "</ul>";




            message += list;
            grade = jsoneRespone.grade;

            if(grade == "fail")
                $('#message-box-default').addClass("message-box-danger");
            else if(grade == "pass")
                $('#message-box-default').addClass("message-box-warning");
            else if(grade == "Good" && grade == "excellent")
                $('#message-box-default').addClass("message-box-success");

            $('.mb-content').html("<div>"+message+"</div>" );

            $('#display-msg').click();
            $("#quiz input").prop("disabled", true);


        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus)
        }
    });
    e.preventDefault(); //Prevent Default action.
    //e.unbind();
});
