<?php
/**
 * Created by PhpStorm.
 * User: sohib
 * Date: 3/31/15
 * Time: 11:33 AM
 */

class PrintLine implements ProgramTester
{
    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return true;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
    }

    public function before($source)
    {

    }

    public function after($process, $pipes)
    {
        $this->program_output = fgets($pipes[1]);
        $this->output_okay = preg_match("#My first line of C!#", $this->program_output);
    }
}

$pt = new PrintLine();