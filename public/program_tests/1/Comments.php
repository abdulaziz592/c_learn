<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/28/15
 * Time: 3:40 AM
 */

class Comments implements ProgramTester
{
    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return true;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
    }

    public function before($source)
    {

        $this->source_code_okay = false;

        $source = explode("\n", $source);

        foreach($source as $line)
        {
            if(preg_match("/\/\/.*/",$line) || preg_match("/\/\*++.*\*+\//",$line)){
                $this->source_code_okay = true;
                break;
            }
        }

    }

    public function after($process, $pipes)
    {
        $output = [];
        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }
        $this->program_output = implode("", $output);
        $this->output_okay = true;

    }
}

$pt = new Comments();