<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/28/15
 * Time: 4:29 AM
 */

class Mltiplayby10 implements ProgramTester
{
    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
    }

    public function before($source)
    {

        $this->source_code_okay;



    }

    public function after($process, $pipes)
    {


        $this->output_okay = false;

        $n1 = rand(1,100);
        $n2 = rand(1,100);

        $output = [];

        fputs($pipes[0], "$n1\n");
        fputs($pipes[0], "$n2\n");

        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }

        $user_solution = "";
        if(preg_match('/(\d+)/', end($output), $match))
        {
            $user_solution = $match[0];
            if($user_solution == (($n1+$n2)*10))
                $this->output_okay = true;
        }

        $this->message .= "The input is $n1 + $n2 * 10<br>";
        $this->message .= "Your output is $user_solution<br>";
        $this->message .= "The expected output is " . (($n1+$n2) * 10);

        $this->program_output = implode("", $output);

    }
}

$pt = new Mltiplayby10();