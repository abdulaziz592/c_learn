<?php
/**
 * Created by PhpStorm.
 * User: sohib
 * Date: 3/31/15
 * Time: 11:33 AM
 */

class Age implements ProgramTester
{
    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
    }

    public function before($source)
    {

        $this->source_code_okay = false;
        $source = explode("\n", $source);
        $variableDeclared = false;
        $variableInitialized = false;
        foreach($source as $line){

            if(preg_match("/^int\s*[A|a][G|g][E|e].*;$/",trim($line))){
                $variableDeclared = true;
            }

            if($variableDeclared){
                if(preg_match("/[A|a][G|g][E|e]\s*=\s*\d+;$/",trim($line))){
                    $variableInitialized = true;
                    $this->source_code_okay = true;
                    break;
                }
            }


        }

        if($this->source_code_okay){

           $this->message = "Good The Variable is Declared And initialized Correctly";

        }else {
            if($variableDeclared){
                if(!$variableInitialized){
                    $this-> message = "Oh! you didn't set any value to Age";

                }
            }else {
                $this-> message = "Oh! you didn't declare variable age with type int";
            }
        }



    }

    public function after($process, $pipes)
    {
        $output = [];
        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }
        $this->program_output = implode("", $output);
        $this->output_okay = true;

    }
}

$pt = new Age();