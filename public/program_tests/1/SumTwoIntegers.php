<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 3/14/15
 * Time: 2:08 PM
 */

class SumTwoIntegers implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
    }

    public function before($source_code)
    {

        /*
         * This is test if the source code contains comment
         */

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);

        foreach($source_code as $line)
        {
            if(preg_match("/^\/\/./", trim($line)))
            {
                $this->source_code_okay = true;
                break;
            }
        }

        if($this->source_code_okay)
            $this->message .= "Good your code contains comment<br>";
        else
            $this->message .= "Bad your code dose not contains comment<br>";

    }

    public function after($process, $pipes)
    {
        /*
        * $this->program_output =  stream_get_contents($pipes[1]);
         *
         * $output = [];

        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }

    */



        $this->output_okay = false;

        $n1 = rand(1,100);
        $n2 = rand(1,100);

        $output = [];

        fputs($pipes[0], "$n1\n");
        fputs($pipes[0], "$n2\n");

        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }

        $user_solution = "";
        if(preg_match('/(\d+)/', end($output), $match))
        {
            $user_solution = $match[0];
            if($user_solution == ($n1+$n2))
                $this->output_okay = true;
        }

        $this->message .= "The input is $n1 + $n2<br>";
        $this->message .= "Your output is $user_solution<br>";
        $this->message .= "The expected output is " . ($n1+$n2);

        $this->program_output = implode("", $output);
    }

}

$pt = new SumTwoIntegers();