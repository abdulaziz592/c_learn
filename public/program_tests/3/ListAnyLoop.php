<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/29/15
 * Time: 1:18 AM
 */
class ListAnyLoop implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);
        foreach($source_code as $line)
        {
            if(preg_match("/continue/",$line)){
                $this->source_code_okay = true;
            }

        }

        if($this->source_code_okay){
            $this->message = "You used <code>continue</code> statement thats good";

        }else {
            $this->message = "You must use <code>continue</code> statement ";

        }




    }

    public function after($process, $pipes)
    {

        $this->output_okay = false;



        $output = [];



        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }

        $expected = [] ;
        for($i=100;$i<=150;$i++){
            if($i == 111 || $i == 122 || $i == 133 || $i == 148 ) {
                continue;
            }
            $expected[] = $i;
        }

        if(count($output) != count($expected)){
            $this->output_okay = false;
        }else{
            $this->output_okay = true;

            for($i=0;$i<count($output);$i++){

                if($expected[$i] !== intval(trim($output[$i]))){
                    $this->output_okay = false;
                    break;
                }
            }




        }
        $this->message .= "</br>";
        if($this->output_okay){
            $this->message .= "Your Output matched the expected output";
        }else{
            $this->message .= "Your Output is wrong doesn't match the expected output";
        }

        $this->program_output = implode("", $output);
    }


}

$pt = new ListAnyLoop();