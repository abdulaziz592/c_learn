<?php
/**
 * Created by PhpStorm.
 * User: sohib
 * Date: 4/28/15
 * Time: 10:21 AM
 */


class ForFromXtoY implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);
        foreach($source_code as $line)
        {
            if(preg_match("/for/",$line)){
                $this->source_code_okay = true;
            }
        }

        if( $this->source_code_okay){

            $this->message = "You used <code>for</code> thats correct";

        }else {
            $this->message ="You must use <code>for</code> statment";

        }



    }

    public function after($process, $pipes)
    {

        $this->output_okay = false;
        $output = [];
        $userOutput = [];
        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
            $userOutput []= trim($f) ;
        }

        $expected = array("1","2","3","4","5");

        for($i =0 ; $i < count($expected) ; $i++){
            if(count($userOutput) != count($expected))
                break;

            if($userOutput[$i] != $expected[$i]){
                $this->output_okay = false ;
                break;
            }
            $this->output_okay = true;
        }

        $this->program_output = implode($output);

    }


}

$pt = new ForFromXtoY();