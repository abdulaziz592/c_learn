<?php
/**
 * Created by PhpStorm.
 * User: sohib
 * Date: 4/28/15
 * Time: 11:33 AM
 */
//SumFromXToY

class SumFromXToY implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);
        foreach($source_code as $line)
        {
            if(preg_match("/while/",$line)){
                $this->source_code_okay = true;
            }
        }

        if( $this->source_code_okay){

            $this->message = "You used <code>while</code> thats correct";

        }else {
            $this->message ="You must use <code>while</code> statment";

        }



    }

    public function after($process, $pipes)
    {

        $this->output_okay = false;

        $n1 = rand(1,50);
        $n2 = rand(51,100);

        $output = [];
        $userOutput = [] ;
        fputs($pipes[0], "$n1\n");
        fputs($pipes[0], "$n2\n");

        while($f = fgets($pipes[1]))
        {
            $userOutput = trim($f);
            $output[] = $f;
        }


        $x = $n1;
        $y = $n2;
        $sum = 0 ;
        while($x < $y){

            $sum += $x ++;
        }
        if(isset($output[0])) {
            if (intval($output[0]) == $sum) {
                $this->output_okay = true;
            }
        }


        $this->message .= "The input is $n1 and $n2<br>";
        if(isset($output[0]))
        $this->message .= "Your output is $output[0]<br>";
        $this->message .= "The expected output is " . $sum;

        $this->program_output = implode("", $output);
    }


}

$pt = new SumFromXToY();