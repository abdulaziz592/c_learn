<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/28/15
 * Time: 3:21 PM
 */

class YesDoWhile implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {
        $this->source_code_okay = false;
        $source_code = explode("\n", $source_code);
        foreach($source_code as $line){

            if(preg_match("/do/",$line)){
                $this->source_code_okay = true;
            }

        }

        if($this->source_code_okay){
            $this->message = "Good thing you used <code>do .. while</code>";
        }else {
            $this->message = "You must  use  you use <code>do .. while</code> to complete this exersice";

        }

    }

    public function after($process, $pipes)
    {
        $this->output_okay = false;
        $output = [] ;
        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }



            if(count($output) != 2)
                $this->output_okay = false;
            else
                $this->output_okay = preg_match("/[Yy][Ee][Ss]/",$output[0]) && preg_match("/[Yy][Ee][Ss]/",$output[1]);

            $this->message .= "<br>";
                if($this->output_okay)
                    $this->message .= "Output is True :) Nice job";
                else
                    $this->message .= "Output is False :(, make sure you output YES tow times with \n";



        $this->program_output = implode($output);



    }


}

$pt = new YesDoWhile();