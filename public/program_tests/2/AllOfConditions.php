<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/28/15
 * Time: 6:44 AM
 */

class AllOfConditions implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);
        $if= false;
        $else = false;
        $elseif = false;
        foreach($source_code as $line)
        {
            if(preg_match("/if/",$line)){
                $if = true;
            }
            if(preg_match("/else/",$line)){
                $else =  true;
            }
            if(preg_match("/else\s+if/",$line)){
                $elseif = true;
            }
        }
        $this->source_code_okay = $if && $else && $elseif;

        if( $this->source_code_okay){

            $this->message = "very good You used all <code>if</code>,<code>else</code>,<code>else if</code>";

        }else {
            $this->message = "make sure you used all of  <code>if</code>,<code>else</code>,<code>else if</code>";

        }



    }

    public function after($process, $pipes)
    {
        $this->output_okay = true;

        $output = fgets($pipes[1]);

        $this->program_output = $output;

    }

}

$pt = new AllOfConditions();