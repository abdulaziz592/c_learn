<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/28/15
 * Time: 5:46 AM
 */

class DirectCompare implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);

        foreach($source_code as $line)
        {
            if(preg_match("/\d+\s*(?:>|<|>=|<=|!|!=)\s*\d+;/",$line)){
                $this->source_code_okay = true;
                break;
            }
        }
        if($this->source_code_okay)
        $this->message = "Yes you got it right";
        else
            $this->message = "Your Doing it Wrong just compare tow numbers directly dent forget to put semicolon at the end ";



    }

    public function after($process, $pipes)
    {

        $output = [];
        while($f = fgets($pipes[1]))
        {
            $output[] = $f;
        }
        $this->program_output = implode("", $output);
        $this->output_okay = true;

    }

}

$pt = new DirectCompare();