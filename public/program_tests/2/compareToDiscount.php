<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 3/14/15
 * Time: 2:08 PM
 */

class compareToDiscount implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {
    }

    public function after($process, $pipes)
    {
        $this->output_okay = false;

        $output = fgets($pipes[1]);

        $user_solution = "";
        if(preg_match('/You get a 10 prcent discount/', $output, $match))
        {
            $user_solution = $match[0];
            $this->output_okay = true;
        }

        $this->message .= "Your output is $user_solution<br>";
        $this->message .= "The expected output is \"You get a 10 prcent discount\"";

        $this->program_output = $output;
    }

}

$pt = new compareToDiscount();