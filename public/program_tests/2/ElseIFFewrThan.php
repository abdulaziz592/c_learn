<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/28/15
 * Time: 6:35 AM
 */

class ElseIFFewrThan implements ProgramTester
{

    private $program_output;
    private $message;
    private $source_code_okay;
    private $output_okay;


    public function get_program_output()
    {
        return $this->program_output;
    }

    public function get_message()
    {
        return $this->message;
    }

    public function is_source_code_okay()
    {
        return $this->source_code_okay;
    }

    public function is_output_okay()
    {
        return $this->output_okay;
    }

    public function __construct()
    {
        $this->message = "";
        $this->source_code_okay = true;
    }

    public function before($source_code)
    {

        $this->source_code_okay = false;

        $source_code = explode("\n", $source_code);

        foreach($source_code as $line)
        {
            if(preg_match("/else\s*/",$line)){
                $this->source_code_okay = true;
                break;
            }
        }
        if($this->source_code_okay)
            $this->message = "Yes you got it right";
        else
            $this->message = "Your Doing it Wrong you must use <code>else</code> ";



    }

    public function after($process, $pipes)
    {
        $this->output_okay = false;

        $output = fgets($pipes[1]);

        $user_solution = "";
        if(preg_match('/You get a 5 percent discount!/', $output, $match))
        {
            $user_solution = $match[0];
            $this->output_okay = true;
        }
        if(!$this->output_okay)
            $this->message .= "Your output is Wrong";
        $this->message .= "</br> Your output is $user_solution<br>";
        $this->message .= "The expected output is \"You get a 5 percent discount!\"";

        $this->program_output = $output;

    }

}

$pt = new ElseIFFewrThan();