<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//

	App::singleton('chapters', function(){
		$chapters = chapters::with("sections")->get();
		return $chapters;
	});

	// If you use this line of code then it'll be available in any view
	// as $site_settings but you may also use app('site_settings') as well
	View::share('chapters', app('chapters'));
});


App::after(function($request, $response)
{
	//
});

Route::filter('isAdmin',function(){

    if(Auth::guest()){
        return Redirect::to('/');
    }else {
        if(!Auth::user()->admin){
            return Redirect::to('/');
        }
    }



});
Route::filter('chapters', function(Illuminate\Routing\Route $route, $request){

    $ch = $route->getParameter('sec_id');

   if($route->getName()  === 'tutorial'){
       $ch = sections::find($ch)->chapter_id;
   }



    if(intval($ch) == 1)
        return;

    $maxMcqPercentage = SubmitLog::whereChapterId(intval($ch)-1)->whereUserId(Auth::id())->where('type','=','mcq')->max('percentage');
    $maxProgramPercentage = SubmitLog::whereChapterId(intval($ch)-1)->whereUserId(Auth::id())->where('type','=','program')->max('percentage');
    if($maxMcqPercentage == null) $maxMcqPercentage = 0;
    if($maxProgramPercentage == null) $maxProgramPercentage = 0;

    $totalPercentage =  ($maxMcqPercentage * 0.6) + ($maxProgramPercentage * 0.4);
    if($totalPercentage <= 70){
//        $maxPrecentage = SubmitLog::whereChapterId(intval($ch)-1)->whereUserId(Auth::id())->max('percentage');
//        $userMarks = SubmitLog::whereChapterId(intval($ch)-1)->whereUserId(Auth::id())->wherePercentage($maxPrecentage)->get();
//        if($maxPrecentage == null) $maxPrecentage = 0;
        $message = array(
            "MCQ" => array("percentage" => $maxMcqPercentage,"class" => McqController::getPrcentageClass($maxMcqPercentage * 0.6)),
            "PGIN" => array("percentage" => $maxProgramPercentage , "class" => McqController::getPrcentageClass($maxProgramPercentage * 0.4)),
            "Total" => array("percentage" => $totalPercentage, "class" => McqController::getPrcentageClass($totalPercentage)),
            "chapter" => array("id"=> intval($ch) -1, "title" => chapters::whereId(intval($ch) -1)->get()[0]->title)
            );



        return View::make("message",$message);

    }





});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			//return Redirect::guest('login');
            return Redirect::route('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|.
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
