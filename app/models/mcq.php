<?php
/**
 * Created by PhpStorm.
 * 
 * User: Sohib
 * Date: 2/3/15
 * Time: 8:55 PM
 *
 * @property integer $id
 * @property string $question
 * @property string $correct
 * @property string $option1
 * @property string $option2
 * @property string $option3
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $back_tracking
 * @property string $chapterId
 * @property string $type
 * @property string $difficulty
 * @method static \Illuminate\Database\Query\Builder|\mcq whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereQuestion($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereCorrect($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereOption1($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereOption2($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereOption3($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereBackTracking($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereChapterId($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\mcq whereDifficulty($value)
 * @method static \mcq chapter($type)
 * @method static \mcq easy()
 * @method static \mcq normal()
 * @method static \mcq hard()
 * @method static \mcq mcq()
 * @method static \mcq programInput()
 */

class mcq extends Eloquent{

    protected $table = 'mcq';


    const EASY = "EASY";
    const NORM = "NORMAL";
    const HARD = "HARD";
    const MCQ = 'mcq';
    const PGIN = 'program_Input';


    public function scopeChapter($query, $type)
    {
        return $query->where('chapterId','=',$type);
    }

    public function scopeEasy($query)
    {
        return $query->where('difficulty', '=', mcq::EASY);
    }

    public function scopeNormal($query)
    {
        return $query->where('difficulty', '=', mcq::NORM);
    }

    public function scopeHard($query)
    {
        return $query->where('difficulty', '=', mcq::HARD);
    }

    public function scopeMcq($query)
    {
        return $query->where('type', '=', mcq::MCQ);
    }

    public function scopeProgramInput($query)
    {
        return $query->where('type', '=', mcq::PGIN);
    }
}