<?php

/**
 * Created by PhpStorm.
 * 
 * User: Sohib
 * Date: 3/10/15
 * Time: 3:57 AM
 *
 * @property integer $id
 * @property integer $chapter_id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \chapters $chapter
 * @method static \Illuminate\Database\Query\Builder|\sections whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\sections whereChapterId($value)
 * @method static \Illuminate\Database\Query\Builder|\sections whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\sections whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\sections whereUpdatedAt($value)
 */
class sections extends Eloquent
{
    protected $table = "sections";

    public function chapter (){
        return $this->belongsTo('chapters','chapter_id','id');
    }
}