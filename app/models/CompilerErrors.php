<?php
/**
 * Created by PhpStorm.
 * 
 * User: abdulaziz
 * Date: 3/13/15
 * Time: 11:07 PM
 *
 * @property integer $id
 * @property string $regex
 * @property string $message
 * @property integer $chapter_id
 * @method static \Illuminate\Database\Query\Builder|\CompilerErrors whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CompilerErrors whereRegex($value)
 * @method static \Illuminate\Database\Query\Builder|\CompilerErrors whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\CompilerErrors whereChapterId($value)
 */


class CompilerErrors extends Eloquent
{
    protected $table = 'compiler_errors';
}