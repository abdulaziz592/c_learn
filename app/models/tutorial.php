<?php
/**
 * Created by PhpStorm.
 * 
 * User: Sohib
 * Date: 3/2/15
 * Time: 9:38 PM
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\tutorial whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\tutorial whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\tutorial whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\tutorial whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\tutorial whereUpdatedAt($value)
 */

class tutorial extends Eloquent{
  protected  $table = "tutorial";
}