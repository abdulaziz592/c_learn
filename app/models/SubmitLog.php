<?php
/**
 * Created by PhpStorm.
 * 
 * User: Sohib
 * Date: 4/16/15
 * Time: 1:48 AM
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $difficulty
 * @property integer $chapter_id
 * @property integer $percentage
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog whereDifficulty($value)
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog whereChapterId($value)
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog wherePercentage($value)
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SubmitLog whereUpdatedAt($value)
 */

class SubmitLog extends Eloquent {
    protected $table = "submitLog";


}