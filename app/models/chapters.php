<?php
/**
 * Created by PhpStorm.
 * 
 * User: Sohib
 * Date: 3/10/15
 * Time: 3:56 AM
 *
 * @property integer $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\sections[] $sections
 * @method static \Illuminate\Database\Query\Builder|\chapters whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\chapters whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\chapters whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\chapters whereUpdatedAt($value)
 */

class chapters extends Eloquent {
    protected $table='chapters';
    protected $fillable = array('title');


    public function sections(){
        return $this->hasMany('sections', 'chapter_id');
    }


}