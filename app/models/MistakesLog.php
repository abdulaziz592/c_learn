<?php
/**
 * Created by PhpStorm.
 * 
 * User: abdulaziz
 * Date: 4/14/15
 * Time: 6:36 AM
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $chapter_id
 * @property integer $section_id
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereChapterId($value)
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereSectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MistakesLog whereUpdatedAt($value)
 */


Class MistakesLog extends Eloquent
{
    protected $table = "mistakes_log";
}