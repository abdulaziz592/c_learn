<?php
/**
 * Created by PhpStorm.
 * 
 * User: abdulaziz
 * Date: 4/21/15
 * Time: 11:17 AM
 *
 * @property integer $id 
 * @property integer $chapter_id 
 * @property string $file_name 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property string $Question 
 * @method static \Illuminate\Database\Query\Builder|\Program whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Program whereChapterId($value)
 * @method static \Illuminate\Database\Query\Builder|\Program whereFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\Program whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Program whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Program whereQuestion($value)
 */


class Program extends Eloquent
{
    protected $table = "programs";
}