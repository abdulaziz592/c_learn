<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as'=>'root',function () {

    if(Auth::guest())
        return View::make('landing');

    return Redirect::route('dashboard');

}));




Route::group(array('before'=>'auth'), function()
{
    Route::post('compile/{program_id}', array('uses' => 'HomeController@compile', 'as' => 'compile'));



    Route::get('dashboard', array('uses' => 'HomeController@dashboard', 'as' => 'dashboard'));

    Route::get('test', array('uses' => 'HomeController@test'));


    Route::group(array('before'=>'chapters'), function()
    {
        Route::get('mcq/{sec_id}', array('uses' => 'McqController@mcq_get', 'as' => 'mcq'));
        Route::post('mcq/{sec_id}', array('uses' => 'McqController@mcq_post'));
        Route::get('editor/{sec_id}', array('uses' => 'HomeController@editor' , 'as'=> "PGIN"));

        Route::get("tut/{sec_id}", array(
            'uses' => 'TutorialController@tutorial',
            'as' => 'tutorial',
        ));
    });


    Route::group(array('before'=>'isAdmin'),function (){


        Route::get("admin/students",array(
            'as'=> 'studentsData',
            'uses' => 'AdminController@studentsData'
        ));

        Route::get("admin/students/{id}",array(
            'as'=> 'studentData',
            'uses' => 'AdminController@studentData'
        ));

        Route::get("admin/tutorials",array(
            'as'=> 'adminTutorials',
            'uses' => 'AdminController@showTutorials'
        ));

        Route::get("admin/tutorial/{id}",array(
            'as'=> 'editTutorial',
            'uses' => 'AdminController@editTutorial'
        ));
        Route::post("admin/tutorial/{id}",array(
            'as'=> 'editTutorialPost',
            'uses' => 'AdminController@editTutorialPost'
        ));

        Route::get("admin/addtutorial",array(
            'as'=> 'addTutorial',
            'uses' => 'AdminController@addTutorial'
        ));

        Route::post("admin/addtutorial",array(
            'as'=> 'addTutorialPost',
            'uses' => 'AdminController@addTutorialPost'
        ));
        Route::post("admin/removeTutorial",array(
            'as'=> 'removeTutorialPost',
            'uses' => 'AdminController@removeTutorialPost'
        ));


    });

});


Route::group(array('before'=>'guest'), function()
{
    Route::get('login',array(
        'uses' => 'UserController@login',
        'as'   => 'login',
    ));

    Route::post('login',array(
        'uses' => 'UserController@do_login',
        'as'   => 'do_login',
    ));

    Route::get('register',array(
        'uses' => 'UserController@register',
        'as'   => 'register',
    ));

    Route::post('register',array(
        'uses' => 'UserController@do_register',
        'as'   => 'do_register',
    ));

});

Route::post('logout',array(
    'uses' => 'UserController@do_logout',
    'as'   => 'do_logout',
));

