<?php


class Common
{


    /**
     * Create a post link.
     *
     * @param string $url
     * @param string $text
     * @param array $options
     * @return string
     * @static
     */
    public static function post_link($url, $text, $options)
    {
        $token = csrf_token();

        $attr = "";

        foreach($options as $k => $v)
        {
            $attr .= " $k='$v' ";
        }

        $id = rand(1,10000);

        $a = "<a href='' onclick='post_link_$id()' $attr >$text</a>";

        $script = '<script>function post_link_'.$id.'(){$(\'<form action="'.$url.'" method="post"><input type="hidden" name="csrf_token" value="'.$token.'"></input></form>\').appendTo(\'body\').submit();}</script>';

        return $a.$script;
    }

    public static function get_link($url, $text, $options)
    {
        $token = csrf_token();

        $attr = "";

        foreach($options as $k => $v)
        {
            $attr .= " $k='$v' ";
        }

        $id = rand(1,10000);

        $a = "<a href='' onclick='post_link_$id()' $attr >$text</a>";

        $script = '<script>function post_link_'.$id.'(){$(\'<form action="'.$url.'" method="get"></form>\').appendTo(\'body\').submit();}</script>';

        return $a.$script;
    }
}