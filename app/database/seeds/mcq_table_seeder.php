<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 2/3/15
 * Time: 9:14 PM
 */
use Illuminate\Database\Seeder;

class mcq_table_seeder extends Seeder
{

    private $diffculty = array(mcq::EASY,mcq::NORM,mcq::HARD);


    public function addFake($question, $correct, $opt1, $opt2, $opt3)
    {
        return array(
            'chapterId' => 'fake',
            'question' => $question,
            'correct' => $correct,
            'option1' => $opt1,
            'option2' => $opt2,
            'option3' => $opt3,
            'back_tracking' => rand(1, 9),
            'type' => mcq::MCQ,
            'difficulty' => $this->diffculty[array_rand($this->diffculty,1)],
            'created_at' => date('Y-m-d G:i:s'),
            'updated_at' => date('Y-m-d G:i:s'),
        );
    }

    public function addReal($chpterId, $question, $correct, $opt1, $opt2, $opt3,$type , $difficulty,$backTrackingId)
    {
        return array(
            'chapterId' => $chpterId,
            'question' => $question,
            'correct' => $correct,
            'option1' => $opt1,
            'option2' => $opt2,
            'option3' => $opt3,
            'difficulty' => $difficulty,
            'type' => $type,
            'back_tracking' => $backTrackingId,
            'created_at' => date('Y-m-d G:i:s'),
            'updated_at' => date('Y-m-d G:i:s'),
        );
    }

    private function getCorrectAnswerFrom($file_name)
    {
        switch ($file_name) {
            case "ch1/mcq1.c":
                return "3";
            case "ch1/mcq2.c":
                return "12";
            case "ch1/mcq3.c":
                return "x";
            case "ch1/mcq4.c":
                return "24";
            case "ch1/mcq5.c":
                return "hello world! 2";
            case "ch2/mcq1.c":
                return "TRUEFALSE";
            case "ch2/mcq2.c":
                return "TRUE";
            case "ch2/mcq3.c":
                return "you are old";
            case "ch2/mcq4.c":
                return "you are awsome";
            case "ch2/mcq5.c":
                return "if";
            case "ch3/1.c":
                return "012";
            case "ch3/2.c":
                return "01";
            case "ch3/3.c":
                return "0";
            case "ch3/4.c":
                return "01";
            case "ch3/5.c":
                return "124";
            default:
                return "";
        }

    }

    private function getDifficultyFrom($file_name)
    {
        switch ($file_name) {
            case "ch1/mcq1.c":
                return mcq::HARD;
            case "ch1/mcq2.c":
                return mcq::NORM;
            case "ch1/mcq3.c":
                return mcq::NORM;
            case "ch1/mcq4.c":
                return mcq::HARD;
            case "ch1/mcq5.c":
                return mcq::EASY;
            case "ch2/mcq1.c":
                return mcq::NORM;
            case "ch2/mcq2.c":
                return mcq::EASY;
            case "ch2/mcq3.c":
                return mcq::NORM;
            case "ch2/mcq4.c":
                return mcq::HARD;
            case "ch2/mcq5.c":
                return mcq::HARD;
            case "ch3/1.c":
                return mcq::NORM;
            case "ch3/2.c":
                return mcq::NORM;
            case "ch3/3.c":
                return mcq::NORM;
            case "ch3/4.c":
                return mcq::HARD;
            case "ch3/5.c":
                return mcq::HARD;
            default:
                return "";
        }

    }

    private function getBackTrackingIdForm($file_name)
    {
        switch ($file_name) {
            case "ch1/mcq1.c":
                return 5;
            case "ch1/mcq2.c":
                return 4;
            case "ch1/mcq3.c":
                return 5;
            case "ch1/mcq4.c":
                return 5;
            case "ch1/mcq5.c":
                return 4;
            case "ch2/mcq1.c":
                return 6;
            case "ch2/mcq2.c":
                return 6;
            case "ch2/mcq3.c":
                return 7;
            case "ch2/mcq4.c":
                return 9;
            case "ch2/mcq5.c":
                return 9;
            case "ch3/1.c":
                return 11;
            case "ch3/2.c":
                return 12;
            case "ch3/3.c":
                return 13;
            case "ch3/4.c":
                return 12;
            case "ch3/5.c":
                return 14;
            default:
                return 1;
        }

    }
    public function run()
    {
        mcq::truncate();
        // fake
        mcq::insert(array(
            $this->addFake('Which of the following is the correct order of evaluation for the below expression?', '* / % + - =', '= * / % + -', '/ * % - + =', '* % / - + ='),
            $this->addFake('Which of the following is the correct usage of conditional operators used in C?', 'max = a>b ? a>c?a:c:b>c?b:c', 'a>b ? c=30 : c=40;', 'a>b ? c=30;', 'return (a>b)?(a:b)'),
            $this->addFake('Which of the following correctly shows the hierarchy of arithmetic operations in C?', '/ * + -', '/ + * -', '* - / +', '+ - / *'),
            $this->addFake('Which of the following is the correct order if calling functions in the below code?<br><code>a = f1(23, 14) * f2(12/4) + f3();</code>', 'Order may vary from compiler to compiler', 'f1, f2, f3', 'f3, f2, f1', 'None of above'),
            $this->addFake('Which of the following are unary operators in C?  <ol><li>!</li><li>sizeof</li><li>~</li><li>&amp;&amp;</li></ol>', '1, 2, 3', '1, 2', '1, 3', '2, 4')
        ));

        // real
        /*
         * load question from DB as HTML
         * mltiple choices loaded here
         */

        mcq::insert(
            array(
                // EASY
                $this->addReal('1', 'What is the correct value to return to the operating system upon the successful completion of a program?', '0', '1', '-1', 'Programs do not return a value.',mcq::MCQ,mcq::HARD,1),
                $this->addReal('1', 'What is the only function all C programs must contain?', 'main()', 'start()', 'system()', 'program()',mcq::MCQ,mcq::EASY,1),
                $this->addReal('1', 'What punctuation is used to signal the beginning and end of code blocks?', '{ }', '-> and <-', 'BEGIN and END', '( and )',mcq::MCQ,mcq::EASY,1),
                $this->addReal('1', 'What punctuation ends most lines of C code?', ';', '.', ':', '\'',mcq::MCQ,mcq::EASY,1),
                $this->addReal('1', 'Which of the following is a correct comment?', '/* Comment */', '*/ Comments */', '** Comment **', '{ Comment }',mcq::MCQ,mcq::NORM,1),
                $this->addReal('1', 'Which of the following is not a correct variable type?', 'real', 'float', 'int', 'double',mcq::MCQ,mcq::NORM,3),
                $this->addReal('1', 'Which of the following is the correct operator to compare two variables?', '==', ':=', 'equal', '=',mcq::MCQ,mcq::EASY,5),
                // NORMAL
                $this->addReal('1', 'What dose stdio stand for?', 'standard input/output', 'string to detrmine input/output', 'system terminal decoder input/output', 'none of the options',mcq::MCQ,mcq::NORM,1),
                $this->addReal('1', 'What dose %d mean?', 'format for indicating values embedded in strings', 'nothing its just text', 'empty space', 'single digit',mcq::MCQ,mcq::NORM,4),
                $this->addReal('1', 'What is the first function called when the program executed?', 'main()', 'scanf()', 'func()', 'printf()',mcq::MCQ,mcq::NORM,1),
                $this->addReal('1', 'What is the correct variable  declaration?', 'int x;', 'x int;', 'x;', 'int;',mcq::MCQ,mcq::EASY,3),
                $this->addReal('1', '== symbol used for assigning values for variables?', 'false', 'true', 'NULL', 'NULL',mcq::MCQ,mcq::EASY,5),
            ));

//        return "<pre>" . e(File::allFiles(public_path("mcq/ch1"))[0]->getContents()) . "</pre>" ;
            // PHIN all chapters
        foreach (File::allFiles(public_path("mcq")) as $file) {
            $question = "What is the output of the following code?<br>";
            $question .= '<pre><code class="language-c">' . e($file->getContents()) . "</code></pre>";
            $file_name = $file->getRelativePathname();
            $correctAnswer = $this->getCorrectAnswerFrom($file_name);
            $ch = "";

            if(starts_with($file_name,'ch1'))
                $ch = '1';
            elseif(starts_with($file_name,'ch2'))
                $ch = '2';
            elseif(starts_with($file_name,'ch3'))
                $ch = '3';

            mcq::insert($this->addReal($ch, $question, $correctAnswer, "NULL", "NULL", "NULL",mcq::PGIN,$this->getDifficultyFrom($file_name),$this->getBackTrackingIdForm($file_name)));
        }


        mcq::insert(
            array(
            $this->addReal('2','Which of the following is consider as <code>TRUE</code> in C?','1','66','0','-1',mcq::MCQ,mcq::NORM,6),
            $this->addReal('2','Which of the following is the boolean operator for logical-and?','&&','&','||','|',mcq::MCQ,mcq::NORM,9),
            $this->addReal('2','Evaluate <code>!(1 && !(0 || 1))</code>','1','0','the statement is wrong','NULL',mcq::MCQ,mcq::HARD,9),
            $this->addReal('2','Which of the following shows the correct syntax for an if statement?','if ( expression )','if expression','if { expression','expression if',mcq::MCQ,mcq::EASY,7),
            $this->addReal('2','5 >= 4 is?','TRUE','FALSE','NULL','NULL',mcq::MCQ,mcq::EASY,6),
            $this->addReal('2','6 != 2 is ?','TRUE','FALSE','NULL','NULL',mcq::MCQ,mcq::EASY,6),
            $this->addReal('2','How else if is different than else?','else if used when multiple conditional statements that may all evaluate to false.','else if is similar to else.','else if used when multiple conditional statements that may all evaluate to true.','else if used when printing is needed.',mcq::MCQ,mcq::EASY,8),
            $this->addReal('2','One of the following operators is not a relational operator what is it?','!>','==','<=','<',mcq::MCQ,mcq::EASY,6),
            $this->addReal('2','What is the benefits of conditional statements?','change the  flow of the program.','printing values.','used to define new segments of code.','used for input',mcq::MCQ,mcq::EASY,6),
            $this->addReal('2','One of the statements is conditional statements?','if(i==0)','switch(i)','while(i)','printf();',mcq::MCQ,mcq::EASY,7),
            $this->addReal('2','one of the following is FALSE when using it as expression?','44-44','33','-33','44',mcq::MCQ,mcq::NORM,6),
    ));

        // sec 3

        mcq::insert(
            array(
                $this->addReal('3','What is the final value of x when the code <pre><code class="language-c">int x; for(x=0; x<10; x++) {}</code></pre> is run?','10','9','0','1',mcq::MCQ,mcq::HARD,11),
                $this->addReal('3','When does the code block following <pre><code class="language-c" >while(x<100)</code></pre> execute?','When x is less than one hundred','When x is greater than one hundred','When x is equal to one hundred','While it wishes',mcq::MCQ,mcq::EASY,12),
                $this->addReal('3','Which is not a loop structure?','repeat Until','for','do while','while',mcq::MCQ,mcq::EASY,10),
                $this->addReal('3','How many times is a <code>do while</code> loop guaranteed to loop?','1','0','Infinitely','Variable',mcq::MCQ,mcq::NORM,13),
                $this->addReal('3','Why Loops are used?','to repeat a block of code','to call other part of program','to set values for variables','to control the flow of program',mcq::MCQ,mcq::NORM,10),
                $this->addReal('3','What is the correct order for <code>for</code> statement','for(i = 0; i<3;i++)','for(i = 0 ; i++; i < 20)','for(i > 22; i - - ;i = 0 )','for(1)',mcq::MCQ,mcq::EASY,11),
                $this->addReal('3','What is missing form this for loop <pre><code class="language-c">for(int i = 0;i<22;)</code></pre>','The variable update section','variable initialization is missing.','the condition is missing.','other',mcq::MCQ,mcq::NORM,11),
                $this->addReal('3','<code>while</code> loop has variable update section True Or False?','False','True','NULL','NULL',mcq::MCQ,mcq::EASY,12),
                $this->addReal('3','Putting empty condition in <code>while</code> loop is legal?','False','True','NULL','NULL',mcq::MCQ,mcq::EASY,12),
                $this->addReal('3','What the difference between <code>while</code> and <code>do .. while</code>','Loop body is guaranteed to execute at least once in <code> do .. while </code>','<code>do while</code>is more complex than <code> while </code>','<code> while </code> is more like for loop','other',mcq::MCQ,mcq::HARD,13),
                $this->addReal('3','What is the most common error when using <code>do .. while</code> ','not including a trailing semi-colon after the while','putting empty condition.','using variable update section','infinite loop',mcq::MCQ,mcq::HARD,13),
                $this->addReal('3','What <code> break </code> is used for?','exit the most immediately surrounding loop.','controls the flow of loop.','to exit from the whole program.','to print statement outside the loop',mcq::MCQ,mcq::NORM,14),
                $this->addReal('3','What <code> continue </code> is used for?','execute the loop again from the top','exit the most immediately surrounding loop.','call another function in the program','print statement outside the loop',mcq::MCQ,mcq::NORM,14),

            ));




    }
}
