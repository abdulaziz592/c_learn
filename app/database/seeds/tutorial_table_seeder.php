<?php

/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 3/2/15
 * Time: 9:40 PM
 */

class tutorial_table_seeder extends Seeder{


    function get_title_for_file_name($file_name)
    {
        switch ($file_name) {
            case "sec1/1_0.html":
                return "Intro to C";
            case "sec1/1_2.html":
                return "Explaining your Code";
            case "sec1/1_3.html":
                return "Using Variables";
            case "sec1/1_4.html":
                return "Reading input";
            case "sec1/1_5.html":
                return "Variable Modification";
            case "sec2/2_0.html":
                return "If statements in C";
            case "sec2/2_1.html":
                return "Basic If Syntax";
            case "sec2/2_2.html":
                return "Else if";
            case "sec2/2_3.html":
                return "More interesting conditions using boolean operators";
            case "sec3/3_0.html":
                return "Loops";
            case "sec3/3_1.html":
                return "For Loop";
            case "sec3/3_2.html":
                return "While Loop";
            case "sec3/3_3.html":
                return "Do .. While Loop";
            case "sec3/3_4.html":
                return "Break and Continue";
            default:
                return "NULL";
        }
    }

    public function run()
    {
        tutorial::truncate();

        foreach (File::allFiles(public_path("tutorial_html")) as $file) {

            $file_name = $file->getRelativePathname();
            $file_content = $file->getContents();
            $title = $this->get_title_for_file_name($file_name);

            $id = DB::table('sections')->where('title', $title)->first()->id;

            tutorial::insert(array(
                'id' => $id,
                'title' => $title,
                'content' => $file_content,
                'created_at' => date('Y-m-d G:i:s'),
                'updated_at' => date('Y-m-d G:i:s'),
            ));
        }


    }

}