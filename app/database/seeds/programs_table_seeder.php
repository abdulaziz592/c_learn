<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 4/21/15
 * Time: 11:25 AM
 */


class program_table_seeder extends Seeder
{
    public function run()
    {
        Program::truncate();

        $programs = array();
        // ch1
        $programs[] = ["chapter_id" => "1","Question"=>"" ,"file_name" => "SumTwoIntegers",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "1","Question"=>"We've written a little C in the editor to the right but it's not complete! On line 8, type My first line of C! between the \"\"s." ,"file_name" => "PrintLine",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "1","Question"=>"Store Your Age in Variable Called <code>Age</code>" ,"file_name" => "Age",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "1","Question"=>"Add a Comment on The Following Code" ,"file_name" => "Comments",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "1","Question"=>"<ul> <li>Sum the varibels a,b</li> <li>mltiplay the result of summition by 10</li> <li>output the result of the mltpliction using <code>printf</code></li></ul>" ,"file_name" => "Mltiplayby10",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        //ch2
        $programs[] = ["chapter_id" => "2", "Question"=>"change the value of <code>items</code> to print what is inside the <code>if</code> statment","file_name" => "compareToDiscount",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "2", "Question"=>"On line 8, use a comparison operator to compare two numbers directly without <code>if</code> statement . Make sure to end your line of code with a semicolon.","file_name" => "DirectCompare",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "2", "Question"=>"<ul><li>write an <code>else</code> statement to capture the people who are only buying 5 items or fewer.</li><li>In their case, use printf to output \"You get a 5 percent discount!\"
</li><li>Dont enclude the Qouts in the output</li></ul>","file_name" => "ElseIFFewrThan",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "2", "Question"=>"<ul><li>Write any program you want</li><li>You must use <code>if</code></li><li>You must use <code>else</code></li><li>You must use <code>else if</code></li></ul>","file_name" => "AllOfConditions",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        //ch3
        $programs[] = ["chapter_id" => "3", "Question"=>"<ul><li>List Numbers from 1 to 5</li><li>sprete numbers with <code>/n</code></li></ul>","file_name" => "ForFromXtoY",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "3", "Question"=>"<ul><li>Write program thats sum intgers btween x to y</li><li>Do't set any value for x or y just use them the values will be inserted automticaly</li><li>You must use <code>while</code></li></ul>","file_name" => "SumFromXToY",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "3", "Question"=>"<ul><li>Write Program Prints &quot;YES&quot; two times</li><li>Sprete Them with <code>\\n</code> </li><li>You must use <code>Do While</code> for output</li></ul>","file_name" => "YesDoWhile",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];
        $programs[] = ["chapter_id" => "3", "Question"=>"<ul> <li> List numbers From 100 to 150 </li> <li>Put <code>\\n</code> at end of every number</li> <li>Dont list 111,122,133,148</li> <li>You must use<code>continue</code>statment</li></ul>","file_name" => "ListAnyLoop",'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')];

        foreach ($programs as $prog)
        {
            Program::insert($prog);
        }


    }
}