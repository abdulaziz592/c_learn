<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 3/13/15
 * Time: 11:03 PM
 */


class CompilerErrorsSeeder extends Seeder
{
    public function run()
    {
        CompilerErrors::truncate();

        $all_errs = array();
        $all_errs[] = ["regex" => "/error: expected ('|‘);('|’)/", "message" => "You have missed a semicolon \";\" before", "chapter_id" => 1];
        $all_errs[] = ["regex" => "/error: ('|‘).+('|’) undeclared/", "message" => "You have used undeclared variable at", "chapter_id" => 2];
        $all_errs[] = ["regex" => "/error: expected ('|‘)=('|’)/", "message" => "You did not use = sign to assign value to the variable at", "chapter_id" => 3];

        foreach ($all_errs as $err)
        {
            CompilerErrors::insert($err);
        }


    }
}