<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        //when we do truncate with foreign key it will couase an error
        if (App::environment('production'))
            DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints


        // $this->call('UserTableSeeder');

        $this->call('mcq_table_seeder');
        $this->command->info("mcq table seeded!");


        $this->call('ChaptersAndSectionsSeed');
        $this->command->info('ChaptersAndSectionsSeed Seeded!');

        $this->call('tutorial_table_seeder');
        $this->command->info("tutorial table seeded!");

        $this->call('CompilerErrorsSeeder');
        $this->command->info("Compiler Errors table seeded!");

        $this->call('program_table_seeder');
        $this->command->info("programs table seeded!");

    }

}
