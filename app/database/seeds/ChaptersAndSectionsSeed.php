<?php

/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 3/10/15
 * Time: 4:48 AM
 */
class ChaptersAndSectionsSeed extends Seeder
{
    private function addChapters()
    {


             chapters::insert(array('title' => 'Introduction', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
             chapters::insert(array('title' => 'If Statement', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
             chapters::insert(array('title' => 'Loop Statements', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));


    }

    private function addSections()
    {

        sections::insert(array('title' => "Intro to C", 'chapter_id' => '1', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Explaining your Code", 'chapter_id' => '1', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Using Variables", 'chapter_id' => '1', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Reading input", 'chapter_id' => '1', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Variable Modification", 'chapter_id' => '1', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "If statements in C", 'chapter_id' => '2', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Basic If Syntax", 'chapter_id' => '2', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Else if", 'chapter_id' => '2', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "More interesting conditions using boolean operators", 'chapter_id' => '2', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Loops", 'chapter_id' => '3', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "For Loop", 'chapter_id' => '3', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "While Loop", 'chapter_id' => '3', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Do .. While Loop", 'chapter_id' => '3', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));
        sections::insert(array('title' => "Break and Continue", 'chapter_id' => '3', 'created_at' => date('Y-m-d G:i:s'), 'updated_at' => date('Y-m-d G:i:s')));


    }

    public function run()
    {
        chapters::truncate();
        sections::truncate();

        $this->addChapters();
        $this->addSections();


    }

}