<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeQuestionColTypeMcqTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mcq', function(Blueprint $table)
		{
			//
            $table->text('question_new')->nullable();
            $table->dropColumn('question');
		});

        Schema::table('mcq',function (Blueprint $table) {
            $table->renameColumn('question_new','question');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mcq', function(Blueprint $table)
		{
			//
            $table->string('question_new')->nullable();
            $table->dropColumn('question');
		});

        Schema::table('mcq',function (Blueprint $table) {
            $table->renameColumn('question_new','question');
        });
	}

}
