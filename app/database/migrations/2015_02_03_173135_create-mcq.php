<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMcq extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('mcq', function ($mcq_table) {
			$mcq_table -> increments('id');
			$mcq_table -> string('sectionId');
			$mcq_table -> string('question');
			$mcq_table -> string ('correct');
			$mcq_table -> string ('option1');
			$mcq_table -> string ('option2');
			$mcq_table -> string ('option3');
			$mcq_table -> timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('mcq');
	}

}
