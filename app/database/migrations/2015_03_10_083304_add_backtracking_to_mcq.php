<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBacktrackingToMcq extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mcq', function(Blueprint $table)
		{

            $table->integer('back_tracking')->nullable();



		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mcq', function(Blueprint $table)
		{
			//
            $table->dropColumn('back_tracking');
		});
	}

}
