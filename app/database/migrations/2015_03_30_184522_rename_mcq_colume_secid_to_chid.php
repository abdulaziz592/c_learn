<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameMcqColumeSecidToChid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('mcq', function(Blueprint $table)
        {

            $table->renameColumn('sectionId','chapterId');



        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//

        Schema::table('mcq', function(Blueprint $table)
        {

            $table->renameColumn('chapterId','sectionId');



        });
	}

}
