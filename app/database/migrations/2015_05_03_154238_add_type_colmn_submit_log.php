<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColmnSubmitLog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submitLog', function(Blueprint $table)
		{
			//
            $table->enum("type", array('program','mcq'))->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submitLog', function(Blueprint $table)
		{
			//
            $table->dropColumn('type');

        });
	}

}
