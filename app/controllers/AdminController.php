<?php

/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 5/2/15
 * Time: 3:35 AM
 */
class AdminController extends BaseController
{


    public function admin($tabName = "student")
    {
        switch ($tabName) {
            default:
                return Redirect::route('studentsData');
        }
    }

    public function studentsData()
    {
        $vars = ['students' => User::all(array('id', 'first_name', 'last_name', 'email'))];
        return View::make('Admin.students', $vars);
    }

    public function studentData($id)
    {

        $user = User::find($id);
        if ($user == null) {
            return "Page Not Found";

        }

        $mistakes_log = DB::table('mistakes_log')
            ->select(DB::raw('count(*) as count, title, section_id'))
            ->join('sections', function ($join) use ($user) {
                $join->on('mistakes_log.section_id', "=", "sections.id")
                    ->on('user_id', "=", DB::raw($user->id));
            })
            ->groupBy('mistakes_log.section_id')
            ->orderBy('count', 'desc')
            ->get();

        $total_mistakes = DB::table('mistakes_log')
            ->where('user_id', "=", DB::raw($user->id))
            ->count();

        $vars = [
            'user' => $user,
            'mistakes_log' => $mistakes_log,
            'total_mistakes' => $total_mistakes,
        ];

        return View::make('Admin.dashboard', $vars);

    }

    public function showTutorials()
    {
        $vars = [
            'tuts' => tutorial::all()
        ];
        return View::make('Admin.tutorials', $vars);
    }

    public function editTutorial($id)
    {

        $tutorial = tutorial::find($id);
        if ($tutorial == null)
            return "page not found";

        $vars = [
            'tutorial' => $tutorial
        ];
        return View::make('Admin.edittutorial', $vars);
    }

    public function editTutorialPost()
    {
        $input = Input::only(array('id', 'title', 'content'));

        $tutorial = tutorial::find(intval($input['id']));
        if($tutorial == null) Response::json(array('success' => false));
        $section = tutorial::find($input['id']);
        $section->title = $input['title'];
        $tutorial->title = $input['title'];
        $tutorial->content = $input['content'];


        if ($tutorial->save() && $section->save()) {
            return Response::json(array('success' => true));
        } else
            return Response::json(array('success' => false));
    }

    public function addTutorial(){
        return View::make('Admin.newtutorial');
    }

    public function addTutorialPost(){
        $input = Input::only(array('id', 'title', 'content'));

        $chid = $input['id'];
        $title = $input['title'];
        $content = $input['content'];

        if(!chapters::find($chid))
            return Response::json(array('success' => false));
        else{
            $section = new sections();
            $section->chapter_id = intval($chid);
            $section->title = $title;

            $tutorial = new tutorial();
            $tutorial-> title = $title;
            $tutorial->content = $content;

            if($section->save() && $tutorial->save()){
                return Response::json(array('success' => true));

            }

        }
        return Response::json(array('success' => false));
    }

    public function removeTutorialPost(){
        $tutorialId = Input::get('tutid');

        $section = sections::find($tutorialId);
        if($section == null){
            return Response::json(array('success' => false));
        }else {
            $tutorial = tutorial::find($tutorialId);
            if($tutorial == null)
                return Response::json(array('success' => false));

            if($tutorial->delete() && $section->delete()){
                return Response::json(array('success' => true));

            }

        }

        return Response::json(array('success' => false));


    }
}