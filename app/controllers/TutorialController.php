<?php
/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/4/15
 * Time: 4:13 AM
 */

class TutorialController extends BaseController {




    public function tutorial($sec_id)
    {
        $html = tutorial::find($sec_id);
        //$html = File::get(public_path("tutorial_html/sec1/1_0.html"));
        $vars = array(
            'html' => $html->content,
            'sec_id' => $sec_id,
        );


        return View::make('tut',$vars);
    }


}