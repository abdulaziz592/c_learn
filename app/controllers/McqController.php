<?php

/**
 * Created by PhpStorm.
 * User: Sohib
 * Date: 4/4/15
 * Time: 4:11 AM
 */
class McqController extends BaseController
{


    public function fillMcqForChapter($ch, $difficulty)
    {
        $globalTotal = 10; // number of total questions
        $result = []; // final result
        if (mcq::chapter($ch)->count() < $globalTotal)
            $globalTotal = mcq::chapter($ch)->count();
        switch ($difficulty) {
            case mcq::HARD:
                /*
                all hard questions then rest is Normal then Easy
                */
                $result = array_merge($result, mcq::chapter($ch)->hard()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal)->get()->toArray());

                $localTotal = count($result);
                if ($localTotal == $globalTotal)
                    break;
                else {
                    // fill it with normals
                    $result = array_merge($result, mcq::chapter($ch)->normal()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal - $localTotal)->get()->toArray());
                    $localTotal = count($result);
                    if ($localTotal == $globalTotal)
                        break;
                    else {
                        $result = array_merge($result, mcq::chapter($ch)->easy()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal - $localTotal)->get()->toArray());
                    }

                }
                break;
            case mcq::NORM:
                // all normal then 50% hard 50% easy
                $result = array_merge($result, mcq::chapter($ch)->normal()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal)->get()->toArray());
                $localTotal = count($result);
                if ($localTotal == $globalTotal)
                    break;
                else {

                    $hard = mcq::chapter($ch)->hard()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->get()->toArray();
                    $easy = mcq::chapter($ch)->easy()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->get()->toArray();

                    $estimated = $globalTotal - $localTotal;
                    for ($i = 0; $i < $estimated;) {
                        if ($localTotal == $globalTotal)
                            break;
                        if ($i < count($hard)) {
                            $result[count($result)] = $hard[$i];
                            $localTotal++;
                            $i++;
                        }

                        if ($localTotal == $globalTotal)
                            break;

                        if ($i < count($easy)) {
                            $result[count($result)] = $easy[$i];
                            $localTotal++;
                            $i++;
                        }

                    }


                }
                break;
            case mcq::EASY:
                /*
               all Easy questions then rest is Normal then Hard
               */
                $result = array_merge($result, mcq::chapter($ch)->easy()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal)->get()->toArray());

                $localTotal = count($result);
                if ($localTotal == $globalTotal)
                    break;
                else {
                    // fill it with normals
                    $result = array_merge($result, mcq::chapter($ch)->normal()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal - $localTotal)->get()->toArray());
                    $localTotal = count($result);
                    if ($localTotal == $globalTotal)
                        break;
                    else {
                        $result = array_merge($result, mcq::chapter($ch)->hard()->orderBy('id', rand(0, 1) == 1 ? 'acs' : 'decs')->limit($globalTotal - $localTotal)->get()->toArray());
                    }

                }
                break;


                break;
        }
        return $result;
    }

    public static function getPrcentageClass($precentage)
    {
        $precentage = intval($precentage);
        if ($precentage < 60)
            $grade = "danger";
        elseif ($precentage >= 60 && $precentage <= 70)
            $grade = "warning";
        elseif ($precentage > 70 && $precentage <= 80)
            $grade = "info";
        else
            $grade = "success";

        return $grade;

    }

    public function getDifficultyForUser($userId, $chapterId)
    {
        /*
         *
         * if faild at HARD see Normal if faild at Normal see Easy
         *
         */

        if (SubmitLog::whereUserId($userId)->whereChapterId($chapterId)->get()->isEmpty()) {
            return mcq::HARD;
        } else {
            $result = SubmitLog::whereUserId($userId)->whereChapterId($chapterId)->latest()->get(array('difficulty'));
            // dd($result);
            $latestDifficulty = $result[0]->difficulty;


            switch ($latestDifficulty) {
                case mcq::HARD:
                    return mcq::NORM;
                    break;
                case mcq::NORM:
                    return mcq::EASY;
                    break;
                default: // EASY OR ANY THING
                    return mcq::EASY;
            }


        }
    }

    public function mcq_get($sec_id)
    {

        $userId = Auth::id();
        $difficulty = $this->getDifficultyForUser($userId, $sec_id);

        if ($difficulty == null) {
            return Response::json(array("message" => "chapter not found"), 404);
        }

        $mcqs = $this->fillMcqForChapter($sec_id, $difficulty);

        if (count($mcqs) == 0)
            return Response::json(array("message" => "chapter not found"), 404);


        $vars = [
            'mcqs' => $mcqs,
            'sec_id' => $sec_id,
            'difficulty' => $difficulty
        ];


        return View::make('mcq', $vars);
    }

    public function mcq_post($sec_id)
    {

        if (!mcq::chapter($sec_id)->exists()) {
            return Response::json(["error" => "section $sec_id not found"], 400);
        }

        $correct_answers = mcq::chapter($sec_id)->get(array('id', 'correct', 'back_tracking'));
        $user_answers_validation = array();
        $correct_counter = 0;
        foreach ($correct_answers as $answer) {
            $question_id = "Question" . $answer["id"];
            if (Input::has($question_id)) {
                $user_answer = Input::get($question_id);

                if (trim($user_answer) === trim($answer["correct"])) {
                    $user_answers_validation[] = ["id" => $question_id, "result" => true];
                    $correct_counter++;

                } else {

                    $log = new MistakesLog();

                    $log->user_id = Auth::id();
                    $log->chapter_id = $sec_id;
                    $log->section_id = $answer['back_tracking'];
                    $log->type = 'MCQ';

                    $log->save();


                    $user_answers_validation[] = [
                        "id" => $question_id,
                        "result" => false,
                        'back_tracking_url' => route('tutorial', $answer['back_tracking']),
                        'back_tracking_title' => tutorial::find($answer['back_tracking'])['title'],
                    ];

                }
            }
        }

        $total = 10;

        $percentage = ($correct_counter / $total) * 100;
        $response = array();
        $response['answers'] = $user_answers_validation;

        $response['precentage'] = $percentage;

        $grade = "";

        if ($percentage < 60)
            $grade = "Fail";
        elseif ($percentage >= 60 && $percentage <= 70)
            $grade = "Pass";
        elseif ($percentage > 70 && $percentage <= 80)
            $grade = "Good";
        else
            $grade = "Excellent";

        $response['grade'] = $grade;


        $submitLog = new SubmitLog();
        $submitLog->user_id = Auth::id();
        $submitLog->chapter_id = $sec_id;
        $submitLog->percentage = $percentage;
        $submitLog->difficulty = $this->getDifficultyForUser(Auth::id(), $sec_id);
        $submitLog->type = "mcq";
        $submitLog->save();


        return Response::json($response);


    }
}