<?php

class HomeController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function compile($program_id)
    {

        $program = Program::find($program_id);
        $program_name = $program->file_name;
        $source_file = "program_tests/{$program->chapter_id}/$program_name.php";
        include public_path($source_file);

        $com = new compiler();
        $com->compile($_POST['source'], $pt);

        if ($com->compiler_status_code != 0) {
            $msg = implode('<br>', $com->compiler_output);
            $msg = str_replace('  ', '&nbsp&nbsp', $msg);

            $analyzer = new error_analyzer();

            $analyzer->analyze($com->compiler_output);

            $vars = [
                'msg' => $msg,
                'status_code' => $com->compiler_status_code,
                'known_errors' => $analyzer->known_errors,
                'errors_lines' => $analyzer->errors_lines,
            ];

            return View::make('compile', $vars);
        }

        $com->run_program($pt);

        $success = false;
        if ($pt->is_output_okay() and $pt->is_source_code_okay())
            $success = true;


        $submitLog = new SubmitLog();
        $submitLog->user_id = Auth::id();
        $submitLog->chapter_id = $program->chapter_id;
        $submitLog->percentage = $success ? 100 : 0;
        $submitLog->type = "program";
        $submitLog->difficulty = mcq::EASY;
        $submitLog->save();


        $vars = [
            'output' => $com->program_output,
            'status_code' => $com->program_status_code,
            'message' => $pt->get_message(),
            'success' => $success,
        ];

        return View::make('run_program', $vars);
    }

    public function editor($chapter_id)
    {
        $programs = Program::where("chapter_id", '=', $chapter_id)->get()->toArray();
        shuffle($programs);
        $program_name = $programs[0]['file_name'];

        // i just edited the functions to use laravel helper functions instead of php base functions
        //$source_file = public_path() . "/prog/prog.c";
        $source_file = public_path("prog/$chapter_id/$program_name.c");
//        $source_code = file_get_contents($source_file);
        $source_code = File::get($source_file);
        $source_code = e($source_code);
        $question = $programs[0]['Question'];
        $compile_route = route('compile', $programs[0]['id']);

        $vars = array(
            'Question' => $question,
            'source_code' => $source_code,
            'compile_route' => $compile_route,
        );

        return View::make('editor', $vars);
    }

    public function dashboard()
    {

        $mistakes_log = DB::table('mistakes_log')
            ->select(DB::raw('count(*) as count, title, section_id'))
            ->join('sections', function ($join) {
                $join->on('mistakes_log.section_id', "=", "sections.id")
                    ->on('user_id', "=", DB::raw(Auth::id()));
            })
            ->groupBy('mistakes_log.section_id')
            ->orderBy('count', 'desc')
            ->get();

        $total_mistakes = DB::table('mistakes_log')
            ->count();

        $total_mistakes_for_user = DB::table('mistakes_log')
            ->where('user_id', "=", Auth::id())
            ->count();


        $students_chapters_mistakes = DB::table("chapters")
            ->select(DB::raw('count(*) as count, chapters.id'))
            ->leftJoin('mistakes_log', function ($join) {
                $join->on('chapters.id', "=", "mistakes_log.chapter_id");
            })
            ->groupBy("chapter_id")
            ->orderBy("chapters.id")
            ->get();

        $mistake_per_student = DB::table("mistakes_log")
            ->select(DB::raw('count(DISTINCT user_id) as count, chapter_id'))
            ->groupBy("chapter_id")
            ->orderBy("chapter_id")
            ->get();


        $chapters_mistakes = DB::table("mistakes_log")
            ->select(DB::raw('count(*) as count, chapter_id'))
            ->where('user_id', '=', Auth::id())
            ->groupBy("chapter_id")
            ->get();


        $vars = [
            'mistakes_log' => $mistakes_log,
            'total_mistakes' => $total_mistakes,
            'total_mistakes_for_user' => $total_mistakes_for_user,
            'chapters_mistakes' => $chapters_mistakes,
            'students_chapters_mistakes' => $students_chapters_mistakes,
            'mistake_per_student' => $mistake_per_student,
        ];

        return View::make('dashboard', $vars);
    }


    public function test()
    {

        $dom = new DOMDocument('1.0', 'iso-8859-1');

        $rss = $dom->createElement('rss');

        $rss->setAttribute('version', '2.0');

// declaring the namespace media

        $rss->setAttribute('xmlns:media', 'http://search.yahoo.com/mrss/');


// creating an element in the namespace media

        $content = $dom->createElement('xs:ABC');

        $content->setAttribute('attr1', 'val1');
        $content->setAttribute('attr2', 'val2');
        $content->setAttribute('attr3', 'val3');

        $second = $dom->createElement('xs:SECOND');
        $content->appendChild($second);


        $rss->appendChild($content);

        $dom->appendChild($rss);


        $dom->formatOutput = true;

        $vars = array(
            'dom' => $dom,
        );

        $dom->saveXML();

        return View::make('test', $vars);

    }


}
