<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 3/16/15
 * Time: 8:51 PM
 */

class UserController extends BaseController
{


    public function login()
    {
        return View::make('Auth/login');
    }

    public function do_login()
    {
        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        );

        if(!Auth::attempt($credentials, true))
        {
            return Redirect::route('login')->with('message', 'The following errors occurred')->withInput(Input::all());
        }

        return Redirect::route('dashboard');
    }

    public function register()
    {
        return View::make('Auth/register');
    }

    public function do_register()
    {
        $validator = Validator::make(Input::all(), User::$rules);
        if (!$validator->passes())
        {
            return Redirect::route('register')->with('message', 'The following errors occurred')->withErrors($validator)->withInput(Input::all());
        }

        $user = new User(Input::all());
        $user->password = Hash::make(Input::get('password'));
        $user->save();

        Auth::login($user);

        return Redirect::route('tutorial', 1);
    }

    public function do_logout()
    {

        Auth::logout();
        return Redirect::to('/');
    }

}