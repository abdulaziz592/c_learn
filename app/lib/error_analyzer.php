<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 2/1/15
 * Time: 2:12 PM
 */

class error_analyzer {

    public $known_errors;
    public $errors_lines;

    private $all_errs;

    public function __construct()
    {
        $this->load_all_err();
    }

    private function load_all_err()
    {
        $this->all_errs = array();
        $this->all_errs = CompilerErrors::all();
        //$this->all_errs[] = ["regex" => "/error: ('|‘).+('|’) undeclared/", "message" => "You have used undeclared variable at"];
    }

    public function analyze($compiler_errs)
    {
        foreach ($compiler_errs as $err)
        {
            foreach($this->all_errs as $regex)
            {
                if(preg_match($regex["regex"], $err))
                {
                    preg_match_all('/:(\d+)/', $err, $match);

                    $line = $match[1][0];

                    $url = route('tutorial', $regex['chapter_id']);
                    $chapter_name = tutorial::find($regex['chapter_id'])['title'];
                    $related_chapter = "For more information read <a href='$url' class='alert-link'>$chapter_name</a>";

                    $msg = $regex["message"] . " line $line.<br>$related_chapter";

                    $this->known_errors[] = $msg;
                    $this->errors_lines[] = $line;

                    //log the error
                    $section = sections::find($regex['chapter_id']);

                    $log = new MistakesLog();

                    $log->user_id = Auth::id();
                    $log->chapter_id = $section->chapter_id;
                    $log->section_id = $section->id;
                    $log->type = 'program';

                    $log->save();

                    continue;
                }
            }//end foreach all_errs
        }//end foreach compiler_errs
    }//end analyze

}