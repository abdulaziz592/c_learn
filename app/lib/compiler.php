<?php


class compiler
{

    public $compiler_status_code;
    public $compiler_output;

    public $program_status_code;
    public $program_output;


    private $folder;
    private $binary_file;

    /**
     * @param $source_code
     */
    public function compile($source_code, ProgramTester $pt)
    {

        $pt->before($source_code);

        $random = time().'_'.rand();

        $this->folder = public_path()."/prog/$random";
        mkdir($this->folder);

        $source_file = $this->folder."/prog.c";
        file_put_contents($source_file, $source_code);

        $this->binary_file = $this->folder."/prog";

        exec("gcc -std=c99 -o $this->binary_file $source_file 2>&1", $output, $status_code);

        $this->compiler_output = $output;
        $this->compiler_status_code = $status_code;

    }


    public function clean()
    {
        exec("rm -rf " . $this->folder);
    }

    public function run_program(ProgramTester $pt)
    {

        if($this->compiler_status_code != 0 or !file_exists($this->binary_file))
        {
            return;
        }


        $descriptors = array(
            0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
            2 => array("pipe", "w"),  // stderr is a file to write to
        );

        $cmd = $this->binary_file;

        $process = proc_open($cmd, $descriptors, $pipes, $this->folder);

        if (!is_resource($process))
        {
            //error not resource
            $this->program_status_code = 1;
            return;
        }

        $pt->after($process, $pipes);
        $this->program_output = $pt->get_program_output();

        fclose($pipes[0]);
        fclose($pipes[1]);
        fclose($pipes[2]);
        $this->program_status_code =  proc_close($process);
    }



    public function __destruct()
    {
        $this->clean();
    }


}