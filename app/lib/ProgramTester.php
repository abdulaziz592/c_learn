<?php
/**
 * Created by PhpStorm.
 * User: abdulaziz
 * Date: 3/14/15
 * Time: 2:02 PM
 */

interface ProgramTester
{
    public function get_program_output();
    public function get_message();
    public function is_source_code_okay();
    public function is_output_okay();

    //this function will be called before the program compile to run test on the source code
    public function before($source_code);

    //this function will be called after the program compile to run test on the input and out put
    public function after($process, $pipes);
}