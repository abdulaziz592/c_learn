

@if(count($known_errors) > 0)

    <div class="col-xs-12 known-errors">

        @foreach($known_errors as $error)
            <div class="alert alert-danger" role="alert">{{$error}}</div>
        @endforeach
    </div>

@endif

<div class="col-xs-12 comiler-errors">

    {{$msg}}

</div>

<p>Status Code = {{$status_code}}</p>

<script>
    var errors_lines = {{json_encode($errors_lines)}};
</script>