@extends('layout')

@section('breadcrumb')
@parent
<li>Admin</li>
<li><a href="{{URL::route('studentsData')}}">Students Data</a></li>
@stop

@section('content')

    <div class="page-title">
        <h2><a href="{{URL::route('dashboard')}}"><span class="fa fa-arrow-circle-o-left"></span></a> Admin Page</h2>
    </div>

    <div class="col-md-12">

        <!-- START JUSTIFIED TABS -->
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="{{URL::route('studentsData')}}" data-toggle="tab">Students</a></li>
                <li><a href="{{URL::route('adminTutorials')}}" >Tutorials</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab8">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50%">Student First Name</th>
                                <th width="20%">Student Last Name</th>
                                <th width="30%">Student Email</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($students as $student)

                                <tr>
                                    <td><a href="{{URL::route('studentData',array('id'=>$student->id))}}"><strong>{{$student->first_name}}</strong></a></td>
                                    <td><a href="{{URL::route('studentData',array('id'=>$student->id))}}"><strong>{{$student->last_name}}</strong></a></td>
                                    <td>
                                        <a href="mailto:{{$student->email}}"><strong>{{$student->email}}</strong></a>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
        <!-- END JUSTIFIED TABS -->

    </div>
@stop