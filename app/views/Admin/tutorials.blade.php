@extends('layout')

@section('breadcrumb')
@parent
<li>Admin</li>
<li><a href="{{URL::route('adminTutorials')}}">Tutorials</a></li>
@stop
@section('scripts')
    @parent
    {{HTML::script('js/plugins/noty/jquery.noty.js')}}
    {{HTML::script('js/plugins/noty/layouts/topRight.js')}}
    {{HTML::script('js/plugins/noty/themes/default.js')}}

    <script type="text/javascript" rel="script">
        function deleteItem(id) {


            data = {
                tutid:id
            };

            $.post('{{URL::route('removeTutorialPost')}}', data, function (data) {

                if(data.success){
                    noty({text: 'deletion is Successful', layout: 'topRight', type: 'success'});
                    window.location.reload();
                }else {
                    noty({text: 'Submit is failed', layout: 'topRight', type: 'error'});
                }


            });

        }

        function notyConfirm(id){
            noty({
                text: 'Are You sure you want to delete this tutorial?',
                layout: 'topRight',
                buttons: [
                    {addClass: 'btn btn-success btn-clean', text: 'Yes', onClick: function($noty) {
                        deleteItem(id);
                        $noty.close();
                    }
                    },
                    {addClass: 'btn btn-danger btn-clean', text: 'Cancel', onClick: function($noty) {
                        $noty.close();
                    }
                    }
                ]
            })
        }
    </script>
    @stop
@section('content')

    <div class="page-title">
        <h2><a href="{{URL::route('dashboard')}}"><span class="fa fa-arrow-circle-o-left"></span> </a>Admin Page</h2>
    </div>

    <div class="col-md-12">

        <!-- START JUSTIFIED TABS -->
        <div class="panel panel-default tabs">
            <ul class="nav nav-tabs nav-justified">
                <li ><a href="{{URL::route('studentsData')}}" >Students</a></li>
                <li class="active"><a href="{{URL::route('adminTutorials')}}" >Tutorials</a></li>
            </ul>
            <div class="panel-body tab-content">
                <div class="tab-pane active" id="tab8">
                    <a href="{{URL::route('addTutorial')}}"><button class="btn btn-block btn-primary"><span class="fa fa-plus"></span> New Tutorial</button></a>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="50%">Title</th>
                                <th width="20%">updated at</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($tuts as $tut)
                                <tr>
                                    <td>
                                        <a href="{{URL::route('tutorial',array('sec_id'=>$tut->id))}}">{{$tut->title}}</a>
                                    </td>
                                    <td>{{$tut->updated_at}}</td>
                                    <td>
                                        <a href="{{URL::route('editTutorial',array('id'=>$tut->id))}}"><button  class="btn btn-info btn-rounded">Edit</button></a>
                                        <button  onclick="notyConfirm({{$tut->id}})" class="btn btn-danger btn-rounded">Delete</button>
                                    </td>
                                </tr>
                            @endforeach



                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
        <!-- END JUSTIFIED TABS -->


    </div>
@stop