@extends('layout')
@section('breadcrumb')
    @parent
    <li>Admin</li>
    <li><a href="{{URL::route('studentsData')}}">Students Data</a></li>
    <li><a href="{{URL::route('studentData',array('id'=>$user->id))}}">{{$user->first_name." ".$user->last_name}}</a></li>
@stop

@section('content')


<div class="page-title">
    <h2><a href="{{URL::route('studentsData')}}"><span class="fa fa-arrow-circle-o-left"></span></a> {{$user->first_name." ".$user->last_name}}</h2>
</div>

    <div class="col-md-12">





        <div class="panel panel-default">
            <div class="panel-heading ui-draggable-handle">
                <div class="panel-title-box">
                    <h3>Mistakes</h3>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                </ul>
            </div>
            <div class="panel-body panel-body-table">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="50%">Section</th>
                            <th width="20%">Number Of Errors</th>
                            <th width="30%">Percentage</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($mistakes_log as $mistake)

                            <tr>
                                <td><a href="{{route('tutorial', $mistake->section_id)}}"><strong>{{$mistake->title}}</strong></a></td>
                                <td><span class="label label-danger">{{$mistake->count}} errors </span></td>
                                <td>
                                    <strong>{{round(($mistake->count/$total_mistakes)*100, 2)}}%</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: {{($mistake->count/$total_mistakes)*100}}%;">85%</div>
                                    </div>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

@stop