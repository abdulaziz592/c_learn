@extends('layout')

@section('breadcrumb')
    @parent
    <li>Admin</li>
    <li><a href="{{URL::route('adminTutorials')}}">Tutorials</a></li>
@stop

@section('scripts')
    @parent
    {{HTML::script('js/plugins/summernote/summernote.js')}}
    {{HTML::script('js/plugins/noty/jquery.noty.js')}}
    {{HTML::script('js/plugins/noty/layouts/topRight.js')}}
    {{HTML::script('js/plugins/noty/themes/default.js')}}

    <script type="text/javascript" rel="script">
        function submitform() {


            data = {
                id: $('#chid').val(),
                title: $('#title').val(),
                content: $('.summernote').code()
            };

            $.post('{{URL::route('addTutorialPost')}}', data, function (data) {

                if(data.success){
                noty({text: 'Submit is Successful', layout: 'topRight', type: 'success'});
                window.location = "{{URL::route('adminTutorials')}}";
                }else {
                noty({text: 'Submit is failed', layout: 'topRight', type: 'error'});
                }


            });

        }
    </script>
@stop
@section('content')

    <div class="page-title">
        <h2><a href="{{URL::route('adminTutorials')}}"><span class="fa fa-arrow-circle-o-left"></span></a> New Tutorial</h2>
    </div>

    <div class="col-md-12">
        <div class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-heading ui-draggable-handle">
                    <div class="panel-title">
                        New Tutorial
                    </div>

                    <div class="panel-body">

                        <div class="form-group">

                            <label class="col-md-3 col-xs-12 control-label">Tutorial Title</label>

                            <div class="col-md-6 col-xs-12">

                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input id="title" class="form-control" type="text" value="">
                                </div>
                                <span class="help-block">Set Tutorial Title here</span>
                            </div>
                        </div>


                        <div class="form-group">

                            <label class="col-md-3 col-xs-12 control-label">Tutorial Chapter</label>

                            <div class="col-md-6 col-xs-12">

                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>

                                    <select id="chid" class="form-control">
                                        @foreach(chapters::all() as $chapter)
                                            <option value="{{$chapter->id}}">{{$chapter->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Tutorial Content</label>

                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">

                                    <div class="summernote">
                                        <article>

                                            <h1>
                                                Your Title Here
                                            </h1>

                                            <p>Your content HERE</p>
                                        </article>

                                    </div>
                                </div>
                                <span class="help-block">Change Tutorial Text Here </span>
                            </div>

                        </div>


                    </div>

                    <div class="panel-footer">
                        <button onclick="submitform()" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop