@extends('layout')
@section('content')


    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading ui-draggable-handle">
                <div class="panel-title-box">
                    <h3>Mistakes</h3>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                </ul>
            </div>
            <div class="panel-body panel-body-table">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="50%">Section</th>
                            <th width="20%">Number Of Errors</th>
                            <th width="30%">Percentage</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($mistakes_log as $mistake)

                            <tr>
                                <td><a href="{{route('tutorial', $mistake->section_id)}}"><strong>{{$mistake->title}}</strong></a></td>
                                <td><span class="label label-danger">{{$mistake->count}} errors </span></td>
                                <td>
                                    <strong>{{round(($mistake->count/$total_mistakes_for_user)*100, 2)}}%</strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: {{($mistake->count/$total_mistakes_for_user)*100}}%;">85%</div>
                                    </div>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <!-- START Area CHART -->
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading ui-draggable-handle">
                <div class="panel-title-box">
                    <h3>Mistakes Chart Compared to Other Student</h3>
                    <span>lower is better</span>
                </div>
            </div>
            <div class="panel-body">
                <div id="morris-area-compare" style="height: 300px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                </div>
            </div>
        </div>
    </div>
    <!-- END Area CHART -->

    <div class="col-xs-12">
        <!-- START DONUT CHART -->
        <div class="panel panel-default">
            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Donut Chart</h3>
            </div>
            <div class="panel-body">
                <div id="morris-donut-chapters" style="height: 300px;">

                </div>
            </div>
        </div>
        <!-- END DONUT CHART -->
    </div>

    <script>



        Morris.Area({
            element: 'morris-area-compare',
            data: [
               /* { y: '2006', a: 100, b: 90 },
                { y: '2007', a: 75,  b: 65 },
                { y: '2008', a: 50,  b: 40 },
                { y: '2009', a: 75,  b: 65 },
                { y: '2010', a: 50,  b: 40 },
                { y: '2011', a: 75,  b: 65 },
                { y: '2012', a: 100, b: 90 }*/
                    @foreach($students_chapters_mistakes as $m)
                        <?php
                        $you = 0;
                        if(array_key_exists($m->id-1, $chapters_mistakes))
                            $you = $chapters_mistakes[$m->id-1]->count;

                        $number_of_student = 1;
                        if(array_key_exists($m->id-1, $mistake_per_student))
                            $number_of_student = $mistake_per_student[$m->id-1]->count;
                        ?>
                        {chapter: 'Chapter {{$m->id}}', all:{{($m->count/$number_of_student)}}, you:{{$you}} },
                    @endforeach
            ],
            /*
            xkey: 'y',
            ykeys: ['a', 'b'],
            */
            xkey: 'chapter',
            ykeys: ['all', 'you'],
            labels: ['Average Student', 'You'],
            resize: true,
            lineColors: ['#1caf9a', '#FEA223'],
            parseTime:false,
            behaveLikeLine:true,
            fillOpacity:0.3
        });


        Morris.Donut({
            element: 'morris-donut-chapters',
            data: [
                /*{label: "Chapter 1", value: 12},
                {label: "Chapter 2", value: 30},
                {label: "Chapter 3", value: 20}*/
                    @foreach($chapters_mistakes as $m)
                        {label: 'chapter'+{{$m->chapter_id}}, 'value': {{($m->count/$total_mistakes_for_user)*100}} },
                    @endforeach
            ],
            colors: ['#95B75D', '#1caf9a', '#FEA223'],
            formatter: function (value, data) { return value + ' %'; }
        });
    </script>

@stop