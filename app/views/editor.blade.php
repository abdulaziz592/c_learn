

@extends('layout')
@section('breadcrumb')
    @parent
    <li> tutorial section id </li>
    <li> program exercise  </li>
@stop


@section('content')

    <div class="page-title">
        <h2>Chapter 1</h2>
    </div>

    <style type="text/css" >
        #editor {
            min-height: 500px;
        }
    </style>

    <div class="col-xs-12">

        <div class="col-sm-6">

            <div class="panel panel-primary">
                <div class="panel-heading ui-draggable-handle">
                    <h2 class="panel-title" >Do The Following Task : <br>{{$Question}}</h2>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body padding-0">
                    <div id="editor">{{$source_code}}</div>
                </div>

                <div class="panel-footer">
                    <button class="btn btn-success pull-right compile-btn" onclick="compile()"><span class="fa fa-play"></span> Compile</button>
                </div>

            </div>

        </div>



        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">Compiler Output</h3>
                </div>
                <div class="panel-body padding-0">
                    <div id="com-result" style="min-height: 40px"></div>
                </div>

                <div class="panel-footer">

                </div>

            </div>
        </div>

    </div>

    <div class="col-xs-12">


    </div>



    {{HTML::script('js/ace-1.1.3/ace.js')}}
    <script>
        var editor = ace.edit("editor");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/c_cpp");
    </script>


    <script>

        function compile()
        {
            var src = editor.getValue();

            var panel = $("#com-result").parents(".panel");
            panel_refresh(panel);
            $("#com-result").parents(".dropdown").removeClass("open");

            $.post('{{$compile_route}}', {source:src}, function(data){
                $('#com-result').html(data);
                panel_refresh(panel);
                highlight_errors();
            });


        }

        var current_markers = [];

        function highlight_errors() {
            if(errors_lines.length < 1)
                return;

            unhighlight_errors();

            errors_lines.forEach(function(line_number){
                var Range = ace.require("ace/range").Range;
                var marker = editor.session.addMarker(new Range(line_number-1, 0, line_number-1, 144), "errorHighlight", "fullLine");
                current_markers.push(marker);
            });

            errors_lines = [];

        }

        function unhighlight_errors(){
            current_markers.forEach(function(e){
                editor.getSession().removeMarker(e);
            });
            current_markers = [];
        }

        editor.on("change", function(e){
            unhighlight_errors();
        });


    </script>


@stop