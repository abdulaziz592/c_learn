@extends('Auth/layout')

@section('form')

    <div class="login-title"><strong>Log In</strong> to your account</div>
    {{Form::open(array('url'=>route('do_login'), 'class'=>'form-horizontal'))}}
        <div class="form-group">
            <div class="col-md-12">
                {{Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'E-mail'))}}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                {{Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password'))}}
            </div>
        </div>

        <div class="form-group">
            <!-- <div class="col-md-6">
                <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
            </div> -->
            <div class="col-md-12">
                <button class="btn btn-info btn-block">Log In</button>
            </div>
        </div>

        <div class="login-subtitle">
            Don't have an account yet? <a href="{{route('register')}}">Create an account</a>
        </div>
    {{Form::close()}}

@stop