@extends('Auth/layout')

@section('form')

    <div class="login-title">Register new account</div>
    {{Form::open(array('url'=>route('do_register'), 'class'=>'form-horizontal'))}}
    <div class="form-group">
        <div class="col-md-12">
            {{Form::text('first_name', null, array('class'=>'form-control', 'placeholder'=>'First Name'))}}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            {{Form::text('last_name', null, array('class'=>'form-control', 'placeholder'=>'Last Name'))}}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            {{Form::email('email', null, array('class'=>'form-control', 'placeholder'=>'E-mail'))}}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            {{Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password'))}}
        </div>
    </div>


    <div class="form-group">

        <div class="col-md-12">
            <button class="btn btn-info btn-block">Register</button>
        </div>
    </div>

    {{Form::close()}}

@stop