@extends('layout')

@section('styles')
@parent

<style>
    #landing-content,.jumbotron {
        float: none;
        margin: 0 auto;
        text-align: center;
    }
</style>

@stop

@section('content')


    <div class="jumbotron">
    	<div class="container">
    		<h1>Welcome To C-learn</h1>
    		<p> See How You Learn C</p>

    		<p>
    			<a href="{{URL::route('tutorial',array('sec_id' => 1))}}" class="btn btn-primary btn-lg">Start Learning</a>
    		</p>

    	</div>
    </div>


    {{--<div id="landing-content">--}}
    {{--<h1></h1>--}}
    {{--<h2> See How You Learn C </h2>--}}
        {{--<p>--}}
            {{--Here you can learn c from scratch--}}
        {{--</p>--}}



    {{--</div>--}}




@stop