@extends('layout')

@section("styles")
    @parent

    <style type="text/css" rel="stylesheet">

        .true-sign {
            color: green;
            display: none;
        }

        .false-sign {
            color: red;
            display: none;
        }

        .questions {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 1.5em;
            font-weight: normal;
        }
    </style>
    {{HTML::style('css/prismjs-css.css')}}


@stop


@section('breadcrumb')
    @parent
    <li>{{$sec_id}}</li>
    <li>multiple choice</li>
    <li> {{$difficulty}}</li>

@stop
@section('content')

    <div class="col-md-12">


        {{ Form::open(array( 'url' => "mcq/$sec_id", 'id' => 'quiz')) }}

        <ul class="questions" type="none">

            @foreach($mcqs as $question)
                <li>

                    <label class="panel panel-default panel-primary">

                        <div class="panel-heading" style="color: #000000">
                            <span class="glyphicon glyphicon-bookmark"></span> :
                            {{trim($question['question'])}}
                            <ul class="panel-controls">
                                <li class="true-sign">
                                    <span class="fa fa-check-circle-o fa-3x"></span>
                                </li>
                                <li class="false-sign">
                                    <span class="fa fa-times-circle-o fa-3x"></span>
                                </li>
                            </ul>


                        </div>

                        <div class="panel-body">

                            @if($question['type'] != mcq::PGIN)
                                <ul class="answers" type="none">
                                    @unless($question['correct'] == "NULL")

                                        <li class="radio">

                                            <label>

                                                <input type="radio" name="{{"Question".$question['id']}}"
                                                       value="{{$question['correct']}}">
                                                {{$question['correct']}}

                                            </label>

                                        </li>
                                    @endunless

                                    @unless($question['option1'] == "NULL")
                                        <li class="radio">

                                            <label>

                                                <input type="radio" name="{{"Question".$question['id']}}"
                                                       value="{{$question['option1']}}">
                                                {{$question['option1']}}

                                            </label>

                                        </li>
                                    @endunless
                                    @unless($question['option2'] == "NULL")

                                        <li class="radio">

                                            <label>


                                                <input type="radio" name="{{"Question".$question['id']}}"
                                                       value="{{$question['option2']}}">
                                                {{$question['option2']}}

                                            </label>

                                        </li>
                                    @endunless
                                    @unless($question['option3'] == "NULL")

                                        <li class="radio">

                                            <label>

                                                <input type="radio" name="{{"Question".$question['id']}}"
                                                       value="{{$question['option3']}}">
                                                {{$question['option3']}}

                                            </label>
                                        </li>
                                    @endunless

                                </ul>
                            @else
                                <label>
                                    Your Answer :
                                    <input type="text" name="{{"Question".$question['id']}}">
                                </label>
                            @endif

                        </div>
                    </label>
                </li>


            @endforeach
        </ul>

        {{Form::submit(null,array('class' => 'btn btn-default pull-right btn-block btn-lg'))}}

        {{Form::close()}}

    </div>


    <button type="button" class="btn btn-default mb-control" id="display-msg" style="display: none"
            data-box="#message-box-default">Default
    </button>
    1
    <div class="message-box animated fadeIn" id="message-box-default">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-globe"></span> Some <strong>Title</strong></div>
                <div class="mb-content">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at tellus sed mauris mollis
                        pellentesque nec a ligula. Quisque ultricies eleifend lacinia. Nunc luctus quam pretium massa
                        semper tincidunt. Praesent vel mollis eros. Fusce erat arcu, feugiat ac dignissim ac, aliquam
                        sed urna. Maecenas scelerisque molestie justo, ut tempor nunc.</p>
                </div>
                <div class="mb-footer">
                    <button class="btn btn-default btn-lg pull-right mb-control-close">Close</button>
                </div>
            </div>
        </div>
    </div>


    {{HTML::script('js/mcq.js')}}
    {{HTML::script('js/prismjs.js')}}
    <script>


    </script>
@stop