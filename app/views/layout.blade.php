<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>C learn: learn how to C</title>

    @section('styles')
        {{HTML::style('css/theme-default.css')}}
        {{HTML::style('css/style.css'); }}
    @show

    @section('scripts')
        {{HTML::script('js/plugins/jquery/jquery.min.js')}}
        {{HTML::script('js/plugins/jquery/jquery-ui.min.js')}}
        {{HTML::script('js/plugins/bootstrap/bootstrap.min.js')}}
        {{HTML::script('js/plugins/icheck/icheck.min.js')}}
        {{HTML::script('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}
        {{HTML::script('js/plugins.js')}}
        {{HTML::script('js/actions.js')}}
        {{HTML::script('js/plugins/morris/raphael-min.js')}}
        {{HTML::script('js/plugins/morris/morris.min.js')}}

    @show
    <script>
/*
        $.ajax({
            url: 'http://api.randomuser.me/?gender=male',
            dataType: 'json',
            success: function (data) {
                user = data.results[0].user;
                $("#mini-pro").attr("src", user.picture.thumbnail);
                $("#lg-pro").attr("src", user.picture.large);

                console.log(user.picture.thumbnail);
            }
        });
*/
    </script>

</head>


<body class="">

<div class="page-container">

    <div  class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            @section('x-navigation')
                <li class="xn-logo">
                    <a href="{{URL::route('root')}}">C-Learn</a>
                    <a href="#" class="x-navigation-control"></a>
                </li>

                <li class="xn-profile">
                    <a href="#" class="profile-mini">
                        {{--<img id="mini-pro" src="" alt="Img">--}}
                        {{HTML::image('img/clearnlogo.png', "C-learn logo",array('id'=>'mini-pro'))}}
                    </a>

                    <div class="profile">
                        <div class="profile-image">
                            {{--<img id="lg-pro" src="" alt="Img">--}}
                            {{HTML::image('img/clearnlogo.png', "C-learn logo", array('id'=>'lg-pro'))}}

                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name">C-learn</div>
                            <div class="profile-data-title">Learn C Programming Language</div>
                        </div>
                        {{--<div class="profile-controls">--}}
                        {{--<a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>--}}
                        {{--<a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>--}}
                        {{--</div>--}}
                    </div>
                </li>

                <!--
                <li class="active">
                    <a href=""><span class="fa fa-desktop"></span> <span class="xn-text">Chapter 1</span></a>
                </li>

                <li class="">
                    <a href=""><span class="fa fa-desktop"></span> <span class="xn-text">Chapter 2</span></a>
                </li>
                -->

                @foreach($chapters as $ch)
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-desktop"></span> <span
                                    class="xn-text">{{$ch->title}}</span></a>
                        <ul>
                            <li class="xn-openable"><a href="#"><span class="fa fa-book"></span>Tutorial</a>
                                <ul>
                                    @foreach($ch->sections as $se)
                                        <?php if (!isset($sec_id)) $sec_id = -1;?>
                                        <li class="{{($sec_id==$se->id)?'active':''}} "><a
                                                    href="{{route('tutorial', $se->id)}}">{{$se->title}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                            <li><a href="{{URL::route('mcq',$ch->id)}}"><span class="fa fa-dot-circle-o"></span>MCQs</a>
                            </li>
                            <li><a href="{{URL::route('PGIN',$ch->id)}}"><span class="fa fa-pencil"></span>Full
                                    Programs</a></li>
                        </ul>
                    </li>
                @endforeach



            @show
        </ul>

        <script>

            $(function(){
                //
                $selected = $('li.active');
                $selected.parents('li.xn-openable').addClass('active');

                $(".page-sidebar").addClass("page-sidebar-fixed");
                $(".page-sidebar").addClass("scroll").mCustomScrollbar({scrollInertia:300});
                $selected.parents('li.xn-openable').first().click();
                $selected.parents('li.xn-openable').first().click();
                $selected.addClass('active');

            });

        </script>

        <!-- END X-NAVIGATION -->
    </div>


    <div class="page-content">

        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            @unless(Auth::guest())
                <li class="xn-icon-button pull-right">
                    {{Common::post_link(route('do_logout'), '<span class="fa fa-sign-out"></span>', array('class'=>'mb-control', 'data-box'=>'#mb-signout'))}}
                </li>
            @endunless

            @if(Auth::user() != null && Auth::user()->admin)
                <li class="xn-icon-button pull-right">
                    {{Common::get_link(route('studentsData'),'<span class="fa fa-gear"></span>',array('class'=>'mb-control'))}}
                </li>
            @endif


        </ul>

        <ul class="breadcrumb">
            @section('breadcrumb')
                <li><a href="{{URL::route('root')}}">Home</a></li>
            @show
        </ul>

        <div class="page-content-wrap">
            <div class="row">
                @yield('content')
            </div>
        </div>

    </div>


</div>


</body>
</html>