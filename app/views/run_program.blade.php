<style type="text/css" >
    #editor1 {
        min-height: 100px;
    }
</style>

<div>


    <div class="alert alert-success" role="alert">The program compiled successfully.</div>


    <div class="alert alert-{{($success)?'success':'danger'}}" role="alert">{{$message}}</div>

    Output

    <div id="editor1" class="alert alert-info" >{{$output}}</div>

    <br>
    Status Code

    <div class="alert alert-info" >

        {{$status_code}}

    </div>


</div>


<script>

    //for fixing error var is undefined
    errors_lines = [];

    var editor1 = ace.edit("editor1");
    editor1.setTheme("ace/theme/monokai");
    editor1.getSession().setMode("ace/mode/c_cpp");
    editor1.renderer.setShowGutter(false);
    editor1.setReadOnly(true);
</script>