@extends('layout')
@section('styles')
    @parent

    @stop

@section('content')



    <div class="error-container">
        <div class="error-text">You Can't Go To This Chapter</div>
        <div class="error-subtext">

            <table class="table table-bordered">
                <caption> Your Marks in Last Chapter({{$chapter["title"]}}) is less than 70% </caption>
                <tr class="{{$MCQ["class"]}}">
                    <th>Mcq (%60 of total marks)</th>
                    <td>{{$MCQ["percentage"]}}%</td>
                </tr>
                <tr class={{$PGIN["class"]}}>
                    <th>Full Programs (%40 of total marks)</th>
                    <td>{{$PGIN["percentage"]}}%</td>
                </tr>
                <tr class="{{$Total["class"]}}">
                    <th>Total</th>
                    <td>{{$Total["percentage"]}}%</td>
                </tr>
            </table>

        </div>


        <div class="error-actions">
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-info btn-block btn-lg" onClick="document.location.href = '{{URL::route('mcq', array("sec_id" => $chapter["id"]))}}';">To {{$chapter["title"]}} MCQs</button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-primary btn-block btn-lg" onClick="document.location.href = '{{URL::route('PGIN', array("sec_id" => $chapter["id"]))}}'">To {{$chapter["title"]}} FullPrograms</button>
                </div>
            </div>
        </div>
    </div>





@stop