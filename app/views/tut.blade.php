@extends('layout')
@section('styles')
    @parent
    {{HTML::style('css/prismjs-css.css')}}

    <style>
        article {
            margin: 0 auto;

            background: #fff;
        }

        article h1 {
            text-align: center;
            background: #ccc;
            padding: 20px;
        }

        article h1 {
            margin: 0;
        }

        article p, table, h2 {
            clear: left;
            padding: 20px;
        }

        article p, table, h2 {
            color: #000;
            font-size: 160%;
            margin: 0 0 .5em;
        }

        table {
            width: 70%;
            margin-left: 15%;
            margin-right: 15%;
        }


    </style>
@stop


@section('x-navigation')
    @parent
    <li class="">
        {{--<a href=""><span class="fa fa-desktop"></span> <span class="xn-text">Chapter 3</span></a>--}}
    </li>
@stop


@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="row" style="margin-bottom: 2em;">
            <a href="{{URL::route('root')}}">
            <button type="button" class="btn btn-default"><span class="fa fa-arrow-left"></span>Back to Dashboard
            </button>
            </a>
        </div>

        <div class="row">
            {{$html}}
        </div>
    </div>
    {{HTML::script('js/prismjs.js')}}
@stop